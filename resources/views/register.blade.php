<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Form</title>
</head>

<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label for="fName">First name:</label><br /><br />
        <input type="text" name="fName" id="fName"><br /><br />
        <label for="lName">Last name:</label><br /><br />
        <input type="text" name="lName" id="lName"><br /><br />
        <label for="gender">Gender:</label><br /><br />
        <input type="radio" name="gender" id="gender" value="Male">
        <label for="female">Male</label>
        <input type="radio" name="gender" id="gender" value="Female">
        <label for="male">Female</label>
        <input type="radio" name="gender" id="gender" value="Others">
        <label for="others">Others</label>
        <br /><br />
        <label for="nationality">Nationality</label><br /><br />
        <select name="nationality" id="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singapore">Singapore</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select><br /><br />
        <label for="spoken">Language Spoken:</label><br /><br />
        <input type="checkbox" name="spoken1" id="spoken1" value="bIndo">
        <label for="spoken1">Bahasa Indonesia</label><br />
        <input type="checkbox" name="spoken2" id="spoken2" value="bIng">
        <label for="spoken2">English</label><br />
        <input type="checkbox" name="spoken3" id="spoken3" value="bIndo">
        <label for="spoken3">Others</label><br /><br />
        <label for="bio">Bio:</label><br /><br />
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br />
        <input type="submit" value="Sign Up"><br /><br />
    </form>
</body>

</html>