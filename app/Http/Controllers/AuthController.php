<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('register');
    }
    public function datang(Request $request)
    {
        // dd($request->all());
        $fnama = $request["fName"];
        $lnama = $request["lName"];
        return view('welcome', compact('fnama', 'lnama'));
    }
}
    

